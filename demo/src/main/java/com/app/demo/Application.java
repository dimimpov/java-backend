package com.app.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.*;
// import io.geewit.data.jpa.envers.repository.EnversRevisionRepositoryFactoryBean;
// import org.springframework.boot.autoconfigure.jdbc.*;
// import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.CommandLineRunner;

// import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

// @EnableAutoConfiguration
@ComponentScan({ "com.app.demo.*" })

@EntityScan
@EnableJpaRepositories(basePackages = "com.app.demo.repos")

// @EnableJpaAuditing
// @EnableJpaRepositories(repositoryFactoryBeanClass =
// EnversRevisionRepositoryFactoryBean.class)
@PropertySource(value = "application.properties")
@SpringBootApplication

class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner demo() {
		return (args) -> {
			System.out.println("Done---");
		};
	}
}