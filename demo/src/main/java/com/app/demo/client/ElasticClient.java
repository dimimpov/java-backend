package com.app.demo.client;

import com.app.ElasticModels.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component

public class ElasticClient {

    @Value("${elasticsearch.username}")
    private String username;

    @Value("${elasticsearch.password}")
    private String password;

    private HttpClient client;

    public ElasticClient() {
        this.client = new HttpClient("http://127.0.0.1:9200", "elastic", "king2306");
    }

    public String createIndex(String index, String body) {
        String response = client.put(index, body);
        return response;
    }

    public String UpdateDoc(String index, String body) {
        String response = client.post(index, body);
        return response;
    }

    public String createDoc(String doc, String body) {
        String response = client.post(doc, body);
        return response;
    }

    public JSONObject search(String url, String body) {
        JSONObject response = client.postForElastic(url, body);
        return response;
    }

}
