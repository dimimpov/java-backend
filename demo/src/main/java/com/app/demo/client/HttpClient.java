package com.app.demo.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.json.JSONObject;

import java.util.Base64;

import java.nio.charset.Charset;

import org.springframework.http.MediaType;

import com.app.ElasticModels.*;

public class HttpClient {

    // add all of this to config files
    private final String server;
    private final RestTemplate rest;
    private final HttpHeaders headers;

    private HttpStatus status;

    public HttpClient(String url, String username, String password) {
        this.server = url;
        this.rest = new RestTemplate();
        this.headers = new HttpHeaders();
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        this.headers.set("Authorization", authHeader);
        this.headers.setContentType(MediaType.APPLICATION_JSON);
    }

    public JSONObject get(String uri, String body) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
        ResponseEntity<String> responseEntity = rest.exchange(server + uri, HttpMethod.GET, requestEntity,
                String.class);
        JSONObject jsonObj = new JSONObject(responseEntity.getBody());
        return jsonObj;

    }

    public String post(String uri, String json) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, headers);
        ResponseEntity<String> responseEntity = rest.exchange(server + uri, HttpMethod.POST, requestEntity,
                String.class);
        this.setStatus(responseEntity.getStatusCode());
        return responseEntity.getBody();
    }

    public JSONObject postForElastic(String uri, String json) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, headers);
        ResponseEntity<String> responseEntity = rest.exchange(server + uri, HttpMethod.POST, requestEntity,
                String.class);
        this.setStatus(responseEntity.getStatusCode());
        JSONObject jsonObj = new JSONObject(responseEntity.getBody());
        return jsonObj;
    }

    public String put(String uri, String json) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, headers);
        ResponseEntity<String> responseEntity = rest.exchange(server + uri, HttpMethod.PUT, requestEntity,
                String.class);
        this.setStatus(responseEntity.getStatusCode());
        return responseEntity.getBody();
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(final HttpStatus status) {
        this.status = status;
    }
}
