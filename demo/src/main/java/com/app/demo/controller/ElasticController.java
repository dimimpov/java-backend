package com.app.demo.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.Object;

import javax.json.Json;

import org.springframework.beans.factory.annotation.Autowired;

import com.app.demo.services.*;

import com.app.demo.tools.QueryBuilder;
import com.google.gson.JsonObject;
import com.app.ElasticModels.*;

@RestController
public class ElasticController {

    @Autowired
    private BarService barService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private UserService userService;

    @Autowired
    private QueryBuilder queryBuilder;

    public void setProductService(BarService barService) {
        this.barService = barService;
    }

    public void setPositionService(PositionService positionService) {
        this.positionService = positionService;
    }

    public void setQueryBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    @GetMapping("/create_bar_index")
    public String createProductIndex() throws IOException {
        String response = barService.createBarIndex();

        return response;

    }

    @GetMapping("/create_position_index")
    public String createPositionIndex() throws IOException {
        String response = positionService.CreatePositionIndex();
        return response;
    }

    @PostMapping("/add_bar")
    public String addProduct(@RequestBody BarDocument BarDocument) {
        String response = barService.createDocument(BarDocument);
        System.out.println(response);

        // } catch (JsonMappingException e) {
        // e.printStackTrace();

        // } catch (JsonProcessingException e) {
        // e.printStackTrace();

        // }

        // }

        // }

        // securityService.autoLogin(userForm.getUsername(),
        // userForm.getPasswordConfirm());

        return "created";

    }

    @GetMapping("/search_bars")
    public String Search(LonLang lonlan) {

        JSONObject coordinates = new JSONObject(lonlan);

        JSONObject query = queryBuilder.buildDistanceQuery(coordinates, "1km");

        String response = barService.search(query.toString());

        return response;

    }

    @GetMapping("/get_all_bars")
    public String Search() {
        String response = barService.getAllDocuments();
        return response;

    }

    @GetMapping("/match_users")
    public String MatchUsers(LonLang lonLan) {
        String coordinates = lonLan.getLat() + "," + lonLan.getLon();
        JSONObject query = queryBuilder.buildAggregationsQuery(coordinates, "km", 1, null);

        String user = positionService.Search(query.toString());
        return user;
    }

    @GetMapping("/get_users_for_bar")
    public String GetUsersForBar(LonLang lonLan) {
        String coordinates = lonLan.getLat() + "," + lonLan.getLon();
        JSONObject query = queryBuilder.buildAggregationsQuery(coordinates, "km", 1, "24h");
        String users = positionService.Search(query.toString());
        return users;
    }

    @PutMapping("/create_user_index")
    public String CreateUserIndex() throws IOException {
        String response = userService.CreateIndex();
        return response;
    }

    @PostMapping("/user_online")
    public String UserOnline(@RequestBody UserDocument userDocument) {
        JSONObject userObj = new JSONObject(userDocument);
        String response = userService.UpdateUser(userObj);
        return response;
    } // will have user details later

    @PostMapping("user_offline")
    public String UserOffline(@RequestBody UserDocument userDocument) {
        JSONObject userObj = new JSONObject(userDocument);
        String response = userService.UpdateUser(userObj);
        return response;
    }

    @PostMapping("/update_position")
    public String UpdatePosition(@RequestBody PositionDocument PositionDocument) {
        String response = positionService.CreatePositionDocument(PositionDocument);
        // check if a user is inside a bar
        JSONObject coordinates = new JSONObject(PositionDocument.pin.location);

        JSONObject query = queryBuilder.buildDistanceQuery(coordinates, "1m");

        String bars = barService.search(query.toString());

        JSONObject jsonBars = new JSONObject(bars);

        JSONArray barsArray = jsonBars.getJSONArray("bars");

        if (barsArray.length() == 0) {
            return response;
        } else {
            return bars;
        }

    }
}