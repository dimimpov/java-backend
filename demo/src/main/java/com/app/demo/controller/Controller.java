package com.app.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.demo.models.*;
import com.app.demo.services.*;

@RestController
public class Controller {

    @Autowired
    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registration")
    public String registration(@RequestBody MyUser requestUserDetails) {
        // userValidator.validate(userForm, bindingResult);

        // if (bindingResult.hasErrors()) {
        // return "registration";
        // }

        userService.save(requestUserDetails);

        // securityService.autoLogin(userForm.getUsername(),
        // userForm.getPasswordConfirm());

        return "Created";
    }

}