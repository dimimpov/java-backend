package com.app.demo.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import java.util.Date;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;

@Entity

@Table(name = "apikey")
public class ApiKey {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "value")
    String value;

    @CreationTimestamp
    @Column(name = "create_at")
    private Date createDate;

    public String getValue() {
        return value;
    }

}