
package com.app.demo.services;

import java.io.IOException;

import com.app.ElasticModels.*;

public interface BarService {

    String createBarIndex() throws IOException;

    String createDocument(BarDocument doc);

    String search(String body);

    String getAllDocuments();

    // MyUser findByUsername(String username);
}