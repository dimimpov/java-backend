package com.app.demo.services;

import java.io.IOException;

import com.app.ElasticModels.*;

public interface PositionService {

    public String CreatePositionIndex() throws IOException;

    public String CreatePositionDocument(PositionDocument positionDocument);

    public String Search(String query);
}