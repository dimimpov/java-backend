package com.app.demo.services;

import org.springframework.stereotype.Service;
import com.app.demo.client.ElasticClient;

import java.io.IOException;

import com.app.ElasticModels.*;

import org.json.JSONObject;
import org.json.JSONArray;

import com.google.gson.Gson;

@Service
// @Import(ElasticsearchConfig.class)
public class BarServiceImlp implements BarService {

    // private ProductDocument product;

    private ElasticClient client;

    public BarServiceImlp() {
        this.client = new ElasticClient();
    }

    @Override
    public String createBarIndex() throws IOException {

        String results = client.createIndex("/bar",
                "{\n" + "    \"settings\" : {\n" + "        \"number_of_shards\" : 3,\n"
                        + "        \"number_of_replicas\" : 0\n" + "    },\n" + "    \"mappings\" : {\n"
                        + "        \"properties\" : {\n" + "            \"name\" : { \"type\" : \"text\" },\n"
                        + "            \"address\" : { \"type\" : \"text\" },\n"
                        + "            \"postal_code\" : { \"type\" : \"text\" },\n"
                        + "            \"country_code\" : { \"type\" : \"text\" },\n"
                        + "            \"city\" : { \"type\" : \"text\" },\n" + "        \"pin\" : {\n"
                        + "        \"properties\" : {\n" + "             \"location\": {\n"
                        + "             \"type\" :  \"geo_point\" " + "}\n" + "}\n" + "}\n" + "},\n"
                        + "  \"_routing\" : { \"required\": \"true\" }" + "    }\n" + "}\n");

        return results;
    }

    @Override
    public String createDocument(BarDocument body) {
        String city = body.getCity();
        String uri = "bar/_doc?routing=" + city;
        Gson gson = new Gson();
        String json = gson.toJson(body);
        String results = client.createDoc(uri, json);
        return results;
    }

    public String search(String body) {

        JSONArray bars = new JSONArray();

        JSONObject jsonResponse = client.search("bar/_search", body);
        JSONArray arr = jsonResponse.getJSONObject("hits").getJSONArray("hits");
        for (int i = 0; i < arr.length(); i++) {
            JSONObject bar = arr.getJSONObject(i).getJSONObject("_source");
            bars.put(bar);
        }
        JSONObject results = new JSONObject();
        results.put("bars", bars);

        return results.toString();
    }

    public String getAllDocuments() {
        JSONObject jsonResponse = client.search("bar/_search", "");
        JSONArray bars = new JSONArray();
        JSONArray arr = jsonResponse.getJSONObject("hits").getJSONArray("hits");
        for (int i = 0; i < arr.length(); i++) {
            JSONObject bar = arr.getJSONObject(i).getJSONObject("_source");
            bars.put(bar);
        }
        JSONObject results = new JSONObject();
        results.put("bars", bars);

        return results.toString();

    }

}