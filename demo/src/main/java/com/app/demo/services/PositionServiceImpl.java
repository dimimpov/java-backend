package com.app.demo.services;

import java.io.IOException;
import com.app.demo.client.ElasticClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;

import com.app.ElasticModels.*;

@Service
public class PositionServiceImpl implements PositionService {

    private ElasticClient client;

    public PositionServiceImpl() {
        this.client = new ElasticClient();
    }

    @Override
    public String CreatePositionIndex() throws IOException {
        String results = client.createIndex("/position",
                "{\n" + "    \"settings\" : {\n" + "        \"number_of_shards\" : 3,\n"
                        + "        \"number_of_replicas\" : 0\n" + "    },\n" + "    \"mappings\" : {\n"
                        + "        \"properties\" : {\n" + "            \"user_id\" : { \"type\" : \"keyword\" },\n"
                        + "            \"created\" : { \"type\" : \"date\" },\n" + "        \"pin\" : {\n"
                        + "        \"properties\" : {\n" + "             \"location\": {\n"
                        + "             \"type\" :  \"geo_point\" " + "}\n" + "}\n" + "}\n" + "},\n"
                        + "  \"_routing\" : { \"required\": \"true\" }" + "    }\n" + "}\n");

        return results;
    }

    @Override
    public String CreatePositionDocument(PositionDocument positionDocument) {
        String doc = "/position/_doc?routing=" + positionDocument.getUserId();
        Gson gson = new Gson();
        String json = gson.toJson(positionDocument);
        String response = client.createDoc(doc, json);
        return response;
    }

    @Override
    public String Search(String query) {
        Object userToMatch = new Object();
        JSONArray users = new JSONArray();

        JSONObject jsonResponse = client.search("position/_search", query);

        try {
            userToMatch = jsonResponse.getJSONObject("aggregations").getJSONObject("users").getJSONArray("buckets")
                    .get(0);
            return userToMatch.toString();
        } catch (JSONException e) {
            users = jsonResponse.getJSONObject("aggregations").getJSONObject("users").getJSONObject("users")
                    .getJSONArray("buckets");
            return users.toString();
        }

    }

}