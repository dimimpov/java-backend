package com.app.demo.services;

import com.app.demo.models.MyUser;
import com.app.demo.repos.MyUserRepo;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import java.io.IOException;
import com.app.demo.client.*;

import com.app.ElasticModels.UserDocument;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private MyUserRepo userRepository;

    private ElasticClient client;

    public UserServiceImpl() {
        this.client = new ElasticClient();
    }

    @Override
    public void save(MyUser user) {
        // user.setPassword(bCryptPasswordEncoder.encode(User.getPassword()));
        // user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    // @Override
    // public UserDetails loadUserByUsername(String username) throws
    // UsernameNotFoundException {

    // /*
    // * Here we are using dummy data, you need to load user data from database or
    // * other third party application
    // */
    // MyUser user = findByUsername(username);

    // UserBuilder builder = null;
    // if (user != null) {
    // builder =
    // org.springframework.security.core.userdetails.User.withUsername(username);
    // builder.password(user.getPassword());
    // } else {
    // throw new UsernameNotFoundException("User not found.");
    // }

    // return builder.build();
    // }

    @Override

    public String CreateIndex() throws IOException {
        String results = client.createIndex("/users",
                "{\n" + "    \"settings\" : {\n" + "        \"number_of_shards\" : 3,\n"
                        + "        \"number_of_replicas\" : 0\n" + "    },\n" + "    \"mappings\" : {\n"
                        + "        \"properties\" : {\n" + "            \"username\" : { \"type\" : \"keyword\" },\n"
                        + "            \"first_name\" : { \"type\" : \"text\" },\n"
                        + "            \"last_name\" : { \"type\" : \"text\" },\n"
                        + "            \"status\" : { \"type\" : \"text\" }\n" + "},\n"
                        + "  \"_routing\" : { \"required\": \"true\" }" + "    }\n" + "}\n");
        return results;

    }

    public String UpdateUser(JSONObject user) {
        JSONObject body = new JSONObject();
        body.put("doc", user);
        String response = client.UpdateDoc("/users/_update/1?routing=1", body.toString());
        return response;
    }

    private MyUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}