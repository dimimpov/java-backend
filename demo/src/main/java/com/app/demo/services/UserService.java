package com.app.demo.services;

import com.app.demo.models.MyUser;

import org.json.JSONObject;

import java.io.IOException;

public interface UserService {
    void save(MyUser user);

    public String CreateIndex() throws IOException;

    public String UpdateUser(JSONObject userDocument);

    // MyUser findByUsername(String username);

    // UserDetails loadUserByUsername(String username);
}
