package com.app.demo.tools;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class QueryBuilder {

    public JSONObject buildDistanceQuery(JSONObject coordinates, String distance) {
        JSONObject query = new JSONObject();
        JSONObject bool = new JSONObject();
        JSONObject must = new JSONObject();
        JSONObject match_all = new JSONObject();
        match_all.put("match_all", new JSONObject());
        must.put("must", match_all);

        JSONObject filter = new JSONObject();
        JSONObject geo_distance = new JSONObject();

        geo_distance.put("pin.location", coordinates);

        geo_distance.put("distance", distance);

        filter.put("geo_distance", geo_distance);

        must.put("filter", filter);
        bool.put("bool", must);

        query.put("query", bool);

        return query;
    }

    public JSONObject buildAggregationsQuery(String coordinates, String unit, Integer distance, String timeBefore) {
        JSONObject topAggs = new JSONObject();
        JSONObject nestedAggs = new JSONObject();
        JSONObject users = new JSONObject();
        JSONObject terms = new JSONObject();
        terms.put("field", "user_id");
        users.put("terms", terms);

        JSONObject rings = new JSONObject();
        JSONObject geo_distance = new JSONObject();
        geo_distance.put("field", "pin.location");
        geo_distance.put("origin", coordinates.toString());
        geo_distance.put("unit", unit);

        JSONArray ranges = new JSONArray();

        JSONObject ring = new JSONObject();

        ring.put("to", 100);

        ranges.put(ring);

        geo_distance.put("ranges", ranges);

        JSONObject geoQuery = new JSONObject();

        geoQuery.put("geo_distance", geo_distance);

        rings.put("rings", geoQuery);

        users.put("aggs", rings);

        nestedAggs.put("users", users);

        topAggs.put("aggs", nestedAggs);

        if (timeBefore != null) {
            // Add time filter
            JSONObject filter = new JSONObject();
            JSONObject bool = new JSONObject();

            JSONArray filters = new JSONArray();
            JSONObject filterObj = new JSONObject();

            JSONObject range = new JSONObject();

            JSONObject field = new JSONObject();

            String gte = "now-" + timeBefore;

            field.put("gte", gte);
            field.put("lt", "now");

            range.put("created", field);
            filterObj.put("range", range);
            filters.put(filterObj);
            bool.put("filter", filters);
            JSONObject finalAgss = new JSONObject();

            JSONObject totalUsers = new JSONObject();

            filter.put("bool", bool);

            JSONObject aggsObj = new JSONObject();

            aggsObj.put("filter", filter);
            aggsObj.put("aggs", nestedAggs);

            totalUsers.put("users", aggsObj);

            finalAgss.put("aggs", totalUsers);

            finalAgss.put("size", 0);

            return finalAgss;
        }
        return topAggs;

    }
}