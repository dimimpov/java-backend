package com.app.demo.repos;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import com.app.demo.models.*;

@Repository
public interface MyUserRepo extends JpaRepository<MyUser, Long> {
    MyUser findByUsername(String username);

    List<MyUser> findAll();
}