package com.app.ElasticModels;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BarDocument {

    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private String address;

    @JsonProperty("postal_code")
    private String postal_code;

    @JsonProperty("country_code")
    private String country_code;

    @JsonProperty("city")
    private String city;

    @JsonProperty("pin")
    Pin pin;

    static public class Pin {

        @JsonProperty("location")
        Location location;

        public Pin() {
        }

        static public class Location {

            @JsonProperty("lon")
            Float lon;

            @JsonProperty("lat")
            Float lat;

            public void setLon(Float lon) {
                this.lon = lon;
            }

            public Float getLon() {
                return lon;
            }

            public void setLat(Float lat) {
                this.lat = lat;
            }

            public Float getLat() {
                return lat;
            }
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPostalCode(String postalCode) {
        this.postal_code = postalCode;
    }

    public String getPostalCode() {
        return postal_code;
    }

    public String getCountryCode() {
        return country_code;
    }

    public void getCountryCode(String countryCode) {
        this.country_code = countryCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAdress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

}
