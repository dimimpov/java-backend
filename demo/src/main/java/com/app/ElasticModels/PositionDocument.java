package com.app.ElasticModels;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PositionDocument {

    @JsonProperty("userId")
    String user_id;

    String created;

    @JsonProperty("pin")
    public Pin pin;

    public PositionDocument() {
        setCreated();
    }

    public class Pin {

        @JsonProperty("location")
        public Location location;

        public Pin() {
        }

        public class Location {

            @JsonProperty("lon")
            Float lon;

            @JsonProperty("lat")
            Float lat;

            public void setLon(Float lon) {
                this.lon = lon;
            }

            public Float getLon() {
                return lon;
            }

            public void setLat(Float lat) {
                this.lat = lat;
            }

            public Float getLat() {
                return lat;
            }
        }
    }

    public void setCreated() {
        DateTimeFormatter FOMATTER = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        this.created = LocalDateTime.now().format(FOMATTER);
    }

    public String getCreated() {
        return created;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getUserId() {
        return user_id;
    }
}