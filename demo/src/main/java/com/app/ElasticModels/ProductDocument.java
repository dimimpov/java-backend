package com.app.ElasticModels;

import org.springframework.data.elasticsearch.annotations.Document;
import javax.persistence.Id;
import java.util.Map;

@Document(indexName = "Products", type = "mapping")
public class ProductDocument {

    @Id
    private String Id;
    private String Name;
    private Float Price;
    private Float Quantity;
    private String Client;

    private Map<String, Object> properties;

    public void setId(String id) {
        this.Id = id;
    }

    public String getId() {
        return Id;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setPrice(Float price) {
        this.Price = price;
    }

    public Float getPrice() {
        return Price;
    }

    public void setQuantity(Float quantity) {
        this.Quantity = quantity;
    }

    public Float getQuantity() {
        return Quantity;
    }

    public void setClient(String client) {
        this.Client = client;
    }

    public String getClient() {
        return Client;
    }

    public String toJsonForElastic() {
        return "{\n" + "\"name\" : " + Name + ", \"quantity\" " + Quantity + " ,\n" + " \"price\" : " + Price + "}";
    }

    Object getParameter(String key) {
        return properties.get(key);
    }

    void addParameter(String key, Object value) {
        properties.put(key, value);
    }

}
